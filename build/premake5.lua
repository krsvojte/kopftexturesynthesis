--local glfw = os.getenv("GLFW_PATH") -- glfw-3.2.1 
--local glew = os.getenv("GLEW_PATH") -- glew-2.0.0
local glm = os.getenv("GLM_PATH") --glm 0.9.8
local blaze_path = os.getenv("BLAZE_PATH") 

solution "TextureSynthesis"
	configurations { "Debug", "Release" }
	buildoptions { "/openmp /std:c++latest" }
	platforms {"x64"}
	vectorextensions "AVX2"

filter { "platforms:x64" }
    system "Windows"
    architecture "x64"


project "TextureSynthesisLib"
	kind "StaticLib"
	language "C++"
	files { 
  	  "../include/**.h", 
      "../src/library/**.h", 
      "../src/library/**.cpp"      
   	}

   	includedirs {
		"../src/library/",						
		"../include/",
		"../external/",
		blaze_path,
		glm		
	}

	defines {
	--"GLEW_STATIC", 
	"WIN64",
	"DEFINE_EXPORTS",
	"_CRT_SECURE_NO_WARNINGS"
	--"NO_IMGUIFILESYSTEM"
	}

	filter "configurations:Debug"
    	defines { "DEBUG" }
    	flags { "Symbols" }        	

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"

	vectorextensions "SSE2"


project "TextureSynthesis"
	kind "ConsoleApp"
	language "C++"
  	targetdir "../bin/%{cfg.buildcfg}/"

  	files { 
  	  "../include/**.h", 
      "../src/app/**.h", 
      "../src/app/**.cpp"      
   	}
	
	includedirs {
		"../src/app/",						
		"../include/",
		"../external/",
		blaze_path,
		glm		
	}

	libdirs {
    	--glfw .. "/src/%{cfg.buildcfg}/",
    	--glew .. "/lib/%{cfg.buildcfg}/%{cfg.platform}/"    	
   	}

	links { 	    
		"TextureSynthesisLib"
	    --"glfw3",	    
	    --"opengl32"	    
	} 

	defines {
	--"GLEW_STATIC", 
	"WIN64",	
	"_CRT_SECURE_NO_WARNINGS"
	--"NO_IMGUIFILESYSTEM"
	}	

	filter "configurations:Debug"
    	defines { "DEBUG" }
    	flags { "Symbols" }    
    	--links {"glew32sd"}

	filter "configurations:Release"
		defines { "NDEBUG" }
		optimize "On"
		--links {"glew32s"}

