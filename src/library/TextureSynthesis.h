#pragma once


#include "Image.h"
#include "Volume.h"
#include "RandomGenerator.h"

#include <vector>
#include <array>

template <size_t Channels>
class TextureSynthesis {

public:
	struct Params {		
		size_t width = 0;
		size_t height = 0;
		size_t channels = 0;
		unsigned char * data = nullptr;
		size_t resolutionSteps = 0;
		size_t outputSize[3] = {0,0,0};
	};

	struct SynthesisLevel {
		Image<Real,Channels> exemplar;
		Volume volume;		
	};

	TextureSynthesis(const Params & params) :
		rngInt(0, INT_MAX)
	{

		
		


		assert(params.width > 0 && params.height > 0 && params.channels > 0);
		assert(params.resolutionSteps >= 1);

		blaze::StaticVector<size_t, 3> volSize{ params.outputSize[0], params.outputSize[1], params.outputSize[2] };
		blaze::StaticVector<size_t, 2> imgSize{ params.width, params.height };

		for (auto i = 0; i < params.resolutionSteps; i++) {
			//Copy exemplar data as-is for finest scale
			if (i == 0) {
				levels.emplace_back(SynthesisLevel{
					Image<Real,Channels>(imgSize[0], imgSize[1], params.data),					
					Volume(volSize[0], volSize[1], volSize[2], params.channels, nullptr)
				});
			}
			//Down sample previous exemplar for coarse scales
			else {
				levels.emplace_back(SynthesisLevel{					
					Image<Real,Channels>::halfDownsample(levels.back().exemplar),					
					Volume(volSize[0], volSize[1], volSize[2], params.channels, nullptr)
				});
			}

			//Halve the resolution
			volSize /= 2;
			imgSize /= 2;
		}
#ifdef VOLUME_TEST
		{
			testVol = VolumeBlaze<Real, Channels>(params.outputSize[0], params.outputSize[1], params.outputSize[2]);
			auto s = testVol.slice(VolumeBlaze<Real, Channels>::SLICE_X, 64);
			s = levels.front().exemplar;
			
			auto & img = levels.front().exemplar;
			

			for (auto x = 0; x < img.columns(); x++) {
				for (auto y = 0; y < img.rows(); y++) {
					auto a = testVol(64, x, y);
					auto b = img(x, y);
					assert(testVol(64, x, y) == img(x, y));
				}
			}
		}
#endif

		auto & coarseLevel = levels.back();
		auto coarseExemplarN = coarseLevel.exemplar.columns() * coarseLevel.exemplar.rows();
		for (auto i = 0; i < coarseLevel.volume.voxelCount(); i++) {

			//Choose random pixel
			auto exemplarIndex = rngInt.next() % coarseExemplarN;

			//Copy all channels
			for (auto c = 0; c < params.channels; c++) {
				coarseLevel.volume.voxels[i*params.channels + c] = 
					coarseLevel.exemplar(exemplarIndex / coarseLevel.exemplar.columns(), exemplarIndex / coarseLevel.exemplar.rows())[c];
			}
		}		
	
	}


	VolumeBlaze<Real, Channels> testVol;

	std::vector<SynthesisLevel> levels;

	RNGUniformInt rngInt;


};
