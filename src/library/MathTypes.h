#pragma once



#include <blaze/Blaze.h>

#ifdef TEXTURE_SYNTHESIS_DOUBLE
using Real = double;
#else
using Real = float;
#endif

using vec2 = blaze::StaticVector<Real, 2>;
using vec3 = blaze::StaticVector<Real, 3>;
using vec4 = blaze::StaticVector<Real, 4>;

/*
#include <glm/glm.hpp>

#ifdef TEXTURE_SYNTHESIS_DOUBLE
using Real = double;
#else

using Real = float;

using vec2 = glm::vec2;
using vec3 = glm::vec3;
using vec4 = glm::vec4;

#endif*/

