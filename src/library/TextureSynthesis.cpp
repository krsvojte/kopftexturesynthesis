#include "TextureSynthesis.h"
#include "Image.h"
/*

TextureSynthesis::TextureSynthesis(const Params & params)
	: / *_exemplar(
		params.width, params.height, params.channels, params.data
	),	* /
	_rngInt(0,INT_MAX)
{
	
	assert(params.width > 0 && params.height > 0 && params.channels > 0);	

	/ *
		Initialize exemplars and volumes at different scales
	* /
	assert(params.resolutionSteps >= 1);
	blaze::StaticVector<size_t, 3> volSize { params.outputSize[0], params.outputSize[1], params.outputSize[2] };
	blaze::StaticVector<size_t, 2> imgSize { params.width, params.height};

	/ *for (auto i = 0; i < params.resolutionSteps; i++){
		//Copy exemplar data as-is for finest scale
		if (i == 0) {
			_levels.emplace_back(SynthesisLevel{
				Image(imgSize[0], imgSize[1], params.channels, params.data),
				Volume(volSize[0], volSize[1], volSize[2], params.channels, nullptr)
			});
		}
		//Down sample previous exemplar for coarse scales
		else {
			_levels.emplace_back(SynthesisLevel{
				Image::halfDownsample(_levels.back().exemplar, 2),
				Volume(volSize[0], volSize[1], volSize[2], params.channels, nullptr)
			});
		}

		//Halve the resolution
		volSize /= 2;
		imgSize /= 2;
	}

	/ *
		Initialize coarsest volume by random samples from exemplar 
		uses uniform distribution
	* /
	auto & coarseLevel = _levels.back();	
	for (auto i = 0; i < coarseLevel.volume.voxelCount(); i++) {
		
		//Choose random pixel
		auto exemplarIndex = _rngInt.next() % coarseLevel.exemplar.pixelCount();
		
		//Copy all channels
		for (auto c = 0; c < params.channels; c++) {
			coarseLevel.volume.voxels[i*params.channels + c] = coarseLevel.exemplar.pixels[exemplarIndex*params.channels + c];
		}
	}
* /



	char breakpt;
	breakpt = 0;

}*/

