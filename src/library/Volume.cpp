#include "Volume.h"

Volume::Volume(size_t sizeX, size_t sizeY, size_t sizeZ, size_t channels, unsigned char * data /* = nullptr*/) :
	size{ sizeX, sizeY, sizeZ }, channels(channels),
	_xzSize(sizeX * sizeZ),
	_totalVoxels(_xzSize * sizeY)
{
	auto N = _totalVoxels * channels;
	voxels.resize(N, 0.0f);

	if (data != nullptr) {
		for (auto i = 0; i < N; i++) {
			voxels[i] = (data[i] / 255.0f);
		}
	}
}

const Real & Volume::get(size_t x, size_t y, size_t z, size_t channel) const
{
	assert(x < size[0] && y < size[1] && z < size[2] && channel < channels);
	return voxels[
		channel +
		channels * (
			+ x 
			+ y * size[0] 
			+ z * _xzSize		
		)
	];
}

