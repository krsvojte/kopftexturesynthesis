#pragma once

#include "MathTypes.h"
#include <vector>

template <typename T, size_t Channels>
struct Image : 
	public blaze::DynamicMatrix<
			blaze::StaticVector<T, Channels, blaze::columnVector>,
			blaze::rowMajor> 	
{

	using Elem = blaze::StaticVector<T, Channels, blaze::columnVector>;
	using Parent = blaze::DynamicMatrix<Elem, blaze::rowMajor>;
	size_t channels = Channels;

public:
	Image(size_t width, size_t height, unsigned char * data = nullptr) 
		: Parent(height, width, Elem(0))
	{

		if (data == nullptr) return;
		auto N = width * height;
		
		#pragma omp parallel for
		for (auto row = 0; row < rows(); row++) {
			for(auto col = 0; col < columns(); col++){
				auto & pixel = (*this)(row, col);
				for (auto c = 0; c < Channels; c++) {
					pixel[c] = data[(row * width + col) * Channels + c] / 255.0f;
				}
			}		
		}
	}
	

	static Image halfDownsample(const Image & src) {
		assert(src.columns() > 2 && src.rows() > 2);
		

		Image img(src.columns() / 2, src.rows() / 2);	 
		
		#pragma omp parallel for
		for (auto row = 0; row < img.rows(); row++) {
			for (auto col = 0; col < img.columns(); col++) {
				auto & pixel = img(row, col);
				for (auto i = 0; i < 2; i++) {
					for (auto j = 0; j < 2; j++) {
						pixel += src(2 * row + i, 2 * col + j);
					}
				}			

				pixel /= T(4);
			}
		}

		return img;
	}

	std::vector<unsigned char> toByteVector() const {
		std::vector<unsigned char> res(columns() * rows() * Channels);

		#pragma omp parallel for
		for (auto row = 0; row < rows(); row++) {
			for (auto col = 0; col < columns(); col++) {				
				for (auto c = 0; c < Channels; c++) {
					res[(row * columns() + col) * Channels + c] = static_cast<unsigned char>((*this)(row, col)[c] * 255);
				}
			}
		}
		return res;		
	}

};