#pragma once

#include "MathTypes.h"
#include <blaze/math/CustomVector.h>
#include <vector>

template<typename T, size_t Channels>
using VolumeElem = blaze::StaticVector<T, Channels, blaze::columnVector>;

template <typename T, size_t Channels>
using VolumeBase = blaze::DynamicVector<
	VolumeElem<T, Channels>, blaze::columnVector
>;

template <typename T, size_t Channels>
using VolumeSlice = blaze::CustomMatrix<VolumeElem<T, Channels>, blaze::aligned, blaze::unpadded, blaze::rowMajor>;


template <typename T, size_t Channels>
struct VolumeBlaze : public VolumeBase<T,Channels>
{

	using Elem = VolumeElem<T, Channels>;
	using Parent = VolumeBase<T, Channels>;
	

	VolumeBlaze(size_t sizeX = 0, size_t sizeY = 0, size_t sizeZ = 0) :
		Parent(sizeX*sizeY*sizeZ, Elem(0)), size{sizeX, sizeY, sizeZ}
	{						
	}

	enum SliceAxis {
		SLICE_X = 0, //YZ plane
		SLICE_Y = 1, //XZ plane
		SLICE_Z = 2  //XY plane
	};

	const Elem & operator()(size_t x, size_t y, size_t z) const {
		assert(x < size[0] && y < size[1] && z < size[2]);
		return (*this)[			
				+ x
				+ y * size[0]
				+ z * size[0] * size[2]
			];		
	}

	Elem & operator()(size_t x, size_t y, size_t z) {
		assert(x < size[0] && y < size[1] && z < size[2]);
		return (*this)[
			+x
				+ y * size[0]
				+ z * size[0] * size[2]
		];
	}



	VolumeSlice<T,Channels> slice(SliceAxis axis, size_t i) {
		assert(i < size[axis]);
		switch (axis) {
		case SLICE_X:
			return VolumeSlice<T, Channels>(this->data() + i, size[1], size[2], size[0] * size[2]);
		case SLICE_Y:
			return VolumeSlice<T, Channels>(this->data() + i * size[0] * size[2], size[2], size[0], size[0]);
		case SLICE_Z:
			return VolumeSlice<T, Channels>(this->data() + i * size[0] * size[1], size[0], size[1], size[0] * size[2]);
		}

		assert(false);
		return VolumeSlice<T, Channels>();
	}
		
	blaze::StaticVector<size_t, 3> size;

};



struct Volume {
	const size_t size[3];
	const size_t channels;
	std::vector<Real> voxels;

	Volume(
		size_t sizeX, 
		size_t sizeY, 
		size_t sizeZ, 
		size_t channels, 
		unsigned char * data = nullptr
	);

	const Real & get(
		size_t x, 
		size_t y, 
		size_t z, 
		size_t channel
	) const;

	template <typename T>
	const T & get(size_t x, size_t y, size_t z) const
	{
		return *(reinterpret_cast<T*>(&get(x, y, z, 0)));
	}

	size_t voxelCount() const {
		return _totalVoxels;
	}

private:
	const size_t _xzSize;	
	const size_t _totalVoxels;

};