
#include <iostream>
#include <cstdio>
#include <fstream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#include "../library/TextureSynthesis.h"

using namespace std;

#define PAUSE_EXIT(i) {stbi_image_free(data); getchar(); return i;}
#define EXIT(i) { return 0; }

int main(int argc, char ** argv) {

	
	
	string path = "../data/Kopf2007/brickwall2.png";

	if (argc > 1) {
		path = argv[0];
	}

	int width, height, channels;
	unsigned char * data = stbi_load(path.c_str(), &width, &height, &channels, 0);

	if (!data) {
		cerr << "Failed to load " << path << "\n";
		PAUSE_EXIT(-1);		
	}

	cout << "Loaded " << path << "\n";
	cout << "w: " << width << ", h: " << height << ", channels: " << channels;
	cout << "\n";


	TextureSynthesis<3>::Params params;
	params.width = width;
	params.height = height;
	params.channels = channels;
	params.data = data;
	params.outputSize[0] = 128;
	params.outputSize[1] = 128;
	params.outputSize[2] = 128;
	
	params.resolutionSteps = 3;

	TextureSynthesis<3> ts(params);


	int k = 0;
	for (auto & level : ts.levels) {
		auto byteVec = level.exemplar.toByteVector();
		char buf[64];
		sprintf_s(buf, "level%d.png", k++);
		stbi_write_png(buf, level.exemplar.columns(), level.exemplar.rows(), level.exemplar.channels, byteVec.data(), 0);
	}


	//Volume export
	std::ofstream f("volume.test");
	for (auto vec : ts.testVol) {		
		for (auto c : vec) {
			char vc = c * 255.0f;
			f.write(&vc, 1);
		}		
	}

	f.close();
	
	EXIT(0);

}